<?php
    //class
    trait Hewan{
        //property
        public $nama;
        public $darah=50;
        public $jumlahKaki;
        public $keahlian;

        public function atraksi(){
            return $this->nama . "Sedang" . $this->keahlian;
        }
    }
    trait Fight{
        public $attackPower;
        public $defencePower;

        public function serang(){

        }

        public function diserang(){

        }
    }

    class Elang{
        use Hewan, Fight;

        public function getInfoHewan(){

        }
    }

    class Harimau{
        use Hewan, Fight;

        public function getInfoHewan(){

        }
    }

    $elang = new Elang();
    $elang->jumlahKaki=2;
    $elang->keahlian="Terbang Tinggi";
    $elang->attackPower=10;
    $elang->defencePower=5;

    $harimau = new Harimau();
    $harimau->jumlahKaki=4;
    $harimau->keahlian="Lari Cepat";
    $harimau->attackPower=7;
    $harimau->defencePower=8;
?>